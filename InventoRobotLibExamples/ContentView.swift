//
//  ContentView.swift
//  InventoRobotLibExamples
//
//  Created by Sreejith K Menon on 06/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
