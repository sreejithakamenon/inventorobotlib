//
//  InventoRobotLib.swift
//  InventoRobotLib
//
//  Created by Sreejith K Menon on 06/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import Foundation

public class InventoRobotLib {

    let name = "InventoRobotLib"
    static public let sharedInstance = InventoRobotLib()
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
