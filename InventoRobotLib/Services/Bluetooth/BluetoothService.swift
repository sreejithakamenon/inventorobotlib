//
//  BluetoothService.swift
//  InventoRobotLib
//
//  Created by Sreejith K Menon on 06/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol BluetoothServiceDelegate: class {
    func bluetoothDidChangeState()
    func bluetoothDidDisconnect(_ peripheral: CBPeripheral, error: NSError?)
    func bluetoothDidReceiveString(_ message: String)
    func bluetoothDidReceiveBytes(_ bytes: [UInt8])
    func bluetoothDidReceiveData(_ data: Data)
    func bluetoothDidReadRSSI(_ rssi: NSNumber)
    func bluetoothDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?)
    func bluetoothDidConnect(_ peripheral: CBPeripheral)
    func bluetoothDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?)
    func bluetoothIsReady(_ peripheral: CBPeripheral)
}

public class BluetoothService: NSObject, BluetoothSerialDelegate {
    
    static public let sharedInstance = BluetoothService()
    
    public var delegate: BluetoothServiceDelegate?
    
    public var commandsDict: [String: String] =
        [
            "Forward": "401",
            "Left": "402",
            "Stop": "403",
            "Right": "404",
            "Reverse": "405",
            "RightHandShake": "406",
            "LeftHandShake": "407",
            "LoofRight": "408",
            "LoofLeft": "409",
            "NormalEyes": "2501",
            "HappyEyes": "2502",
            "ListeningEyes": "2503",
            "SpeakingEyes": "2504",
            "FallbackEyes": "2505",
            "WinkRightEye": "2506",
            "WinkLeftEye": "2507"
    ]
    
    override init() {
        super.init()
        serial = BluetoothSerial(delegate: self)
    }
    
    public func startScan() {
        serial.startScan()
    }
    
    public func stopScan() {
        serial.stopScan()
    }
    
    public func connectToBluetooth(_ peripheral: CBPeripheral) {
        serial.connectToPeripheral(peripheral)
    }
    
    public func disconnect() {
        serial.disconnect()
    }
    
    public func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {
        delegate?.bluetoothDidDiscoverPeripheral(peripheral, RSSI: RSSI)
    }
    
    public func serialDidChangeState() {
        
    }
    
    public func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
        
    }
    
    func sendMessageToRobot(message: String) {
        serial.sendMessageToDevice("d\(message)\r\n")
    }
    
    public func sendCommandToRobot(command: String) {
        if !commandsDict[command]!.isEmpty {
            sendMessageToRobot(message: commandsDict[command]!)
        } else {
            print("RobotCommand not found")
        }
    }
    
    public func serialDidReceiveString(_ message: String) {
        
    }
    
    public func serialDidReceiveBytes(_ bytes: [UInt8]) {
        
    }
    
    public func serialDidReceiveData(_ data: Data) {
        
    }
    
    public func serialDidConnect(_ peripheral: CBPeripheral) {
        self.delegate?.bluetoothDidConnect(peripheral)
    }
}

