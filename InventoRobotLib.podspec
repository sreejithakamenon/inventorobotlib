Pod::Spec.new do |spec|

  spec.name         = "InventoRobotLib"
  spec.version      = "1.0.4"
  spec.summary      = "A CocoaPods library written in Swift for InventoRobot"

  spec.description  = <<-DESC
This CocoaPods library helps you enable Robotic capabilities.
                   DESC

  spec.homepage     = "https://gitlab.com/sreejithakamenon/inventorobotlib"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Sreejith K Menon" => "sreejith@mitrarobot.com" }

  spec.ios.deployment_target = "13.4"
  spec.swift_version = "5"

  spec.source        = { :git => "https://gitlab.com/sreejithakamenon/inventorobotlib", :tag => "#{spec.version}" }
  spec.source_files  = "InventoRobotLib/**/*.{h,m,swift}"

end
