//
//  InventoRobotLibTests.swift
//  InventoRobotLibTests
//
//  Created by Sreejith K Menon on 06/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import XCTest
@testable import InventoRobotLib

class InventoRobotLibTests: XCTestCase {
    
    var inventoRobotLib: InventoRobotLib!

    override func setUp() {
        inventoRobotLib = InventoRobotLib()
    }

    func testAdd() {
        XCTAssertEqual(inventoRobotLib.add(a: 1, b: 1), 2)
    }
    
    func testSub() {
        XCTAssertEqual(inventoRobotLib.sub(a: 2, b: 1), 1)
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
